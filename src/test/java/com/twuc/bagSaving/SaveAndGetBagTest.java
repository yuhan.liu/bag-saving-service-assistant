package com.twuc.bagSaving;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.ArrayList;
import java.util.concurrent.locks.Lock;

import static com.twuc.bagSaving.CabinetFactory.createCabinetWithFullLockers;
import static com.twuc.bagSaving.CabinetFactory.createCabinetWithPlentyOfCapacity;
import static org.junit.jupiter.api.Assertions.*;

class SaveAndGetBagTest extends BagSavingArgument {
    @ParameterizedTest
    @MethodSource("com.twuc.bagSaving.BagSavingArgument#createCabinetWithOnlyOneEmptyLockerAndSavableSizes")
    void should_get_a_ticket_when_saving_a_bag(Cabinet cabinet, BagSize bagSize, LockerSize lockerSize) {
        Ticket ticket = cabinet.save(new Bag(bagSize), lockerSize);
        assertNotNull(ticket);
    }

    @ParameterizedTest
    @MethodSource("com.twuc.bagSaving.BagSavingArgument#createCabinetWithOnlyOneEmptyLockerAndSavableSizes")
    void should_save_and_get_bag_when_locker_is_empty(Cabinet cabinet, BagSize bagSize, LockerSize lockerSize) {
        Bag savedBag = new Bag(bagSize);
        Ticket ticket = cabinet.save(savedBag, lockerSize);
        Bag fetchedBag = cabinet.getBag(ticket);
        assertSame(savedBag, fetchedBag);
    }

    @ParameterizedTest
    @MethodSource({"com.twuc.bagSaving.BagSavingArgument#createSavableBagSizeAndLockerSize"})
    void should_save_smaller_bag_to_bigger_locker(
            BagSize smallerBagSize, LockerSize biggerLockerSize) {
        Cabinet cabinet = createCabinetWithPlentyOfCapacity();

        Bag smallerBag = new Bag(smallerBagSize);
        Ticket ticket = cabinet.save(smallerBag, biggerLockerSize);
        Bag receivedBag = cabinet.getBag(ticket);

        assertSame(smallerBag, receivedBag);
    }

    @ParameterizedTest
    @MethodSource("com.twuc.bagSaving.BagSavingArgument#createNonSavableBagSizeAndLockerSize")
    void should_throw_when_saving_bigger_bag_to_smaller_locker(BagSize biggerBagSize, LockerSize smallerLockerSize) {
        Cabinet cabinet = createCabinetWithPlentyOfCapacity();
        IllegalArgumentException exception =
                assertThrows(IllegalArgumentException.class,
                        () -> cabinet.save(new Bag(biggerBagSize), smallerLockerSize));

        assertEquals(
                String.format("Cannot save %s bag to %s locker.", biggerBagSize, smallerLockerSize),
                exception.getMessage());
    }

    @Test
    void should_throw_if_locker_size_is_not_specified() {
        Cabinet cabinet = createCabinetWithPlentyOfCapacity();
        assertThrows(
                IllegalArgumentException.class,
                () -> cabinet.save(new Bag(BagSize.BIG), null));
    }

    @Test
    void should_throw_if_no_ticket_is_provided() {
        Cabinet cabinet = createCabinetWithPlentyOfCapacity();

        final IllegalArgumentException error = assertThrows(
                IllegalArgumentException.class,
                () -> cabinet.getBag(null));
        assertEquals("Please use your ticket.", error.getMessage());
    }

    @Test
    void should_throw_if_ticket_is_not_provided_by_cabinet() {
        Cabinet cabinet = createCabinetWithPlentyOfCapacity();
        Ticket ticket = new Ticket();

        final IllegalArgumentException exception = assertThrows(
                IllegalArgumentException.class,
                () -> cabinet.getBag(ticket));
        assertEquals("Invalid ticket.", exception.getMessage());
    }

    @ParameterizedTest
    @MethodSource("com.twuc.bagSaving.BagSavingArgument#createSavableBagSizeAndLockerSize")
    void should_throw_if_ticket_is_used(BagSize bagSize, LockerSize lockerSize) {
        Cabinet cabinet = createCabinetWithPlentyOfCapacity();
        Bag bag = new Bag(bagSize);
        Ticket ticket = cabinet.save(bag, lockerSize);
        cabinet.getBag(ticket);

        final IllegalArgumentException exception = assertThrows(
                IllegalArgumentException.class,
                () -> cabinet.getBag(ticket));
        assertEquals("Invalid ticket.", exception.getMessage());
    }

    @ParameterizedTest
    @MethodSource("com.twuc.bagSaving.BagSavingArgument#createSavableBagSizeAndLockerSize")
    void should_throw_if_ticket_is_generated_by_another_cabinet(BagSize bagSize, LockerSize lockerSize) {
        Cabinet anotherCabinet = createCabinetWithPlentyOfCapacity();
        Cabinet cabinet = createCabinetWithPlentyOfCapacity();

        Ticket generatedByAnotherCabinet = anotherCabinet.save(new Bag(bagSize), lockerSize);

        final IllegalArgumentException exception = assertThrows(
                IllegalArgumentException.class,
                () -> cabinet.getBag(generatedByAnotherCabinet));
        assertEquals("Invalid ticket.", exception.getMessage());
    }

    @ParameterizedTest
    @MethodSource("com.twuc.bagSaving.BagSavingArgument#createCabinetWithOnlyOneLockerSizeFull")
    void should_throw_if_correspond_lockers_are_full(
            Cabinet fullCabinet, BagSize bagSize, LockerSize lockerSize) {
        Bag savedBag = new Bag(bagSize);
        InsufficientLockersException error = assertThrows(
                InsufficientLockersException.class,
                () -> fullCabinet.save(savedBag, lockerSize));

        assertEquals("Insufficient empty lockers.", error.getMessage());
    }

    @ParameterizedTest
    @EnumSource(value = LockerSize.class, mode = EnumSource.Mode.EXCLUDE)
    void should_throw_when_saving_nothing(LockerSize lockerSize) {
        Cabinet cabinet = createCabinetWithPlentyOfCapacity();
        final IllegalArgumentException exception = assertThrows(
                IllegalArgumentException.class,
                () -> cabinet.save(null, lockerSize));

        assertEquals("Please at least put something here.", exception.getMessage());
    }

    @Test
    void should_save_bag_and_return_ticket_by_use_stupid_assistant() {
        Bag bag = new Bag(BagSize.MEDIUM);
        StupidAssistant stupidAssistant = new StupidAssistant(createCabinetWithPlentyOfCapacity());
        Ticket ticket = stupidAssistant.saveBag(bag);
        assertNotNull(ticket);
    }

    @Test
    void should_get_bag_by_get_ticket_to_stupid_assistant() {
        Bag bag = new Bag(BagSize.MEDIUM);
        StupidAssistant stupidAssistant = new StupidAssistant(createCabinetWithPlentyOfCapacity());
        Ticket ticket = stupidAssistant.saveBag(bag);
        Bag savedBag = stupidAssistant.getBag(ticket);
        assertEquals(bag, savedBag);
    }

    @Test
    void should_throw_error_when_bag_size_not_allow_with_locker() {
        StupidAssistant stupidAssistant = new StupidAssistant(createCabinetWithPlentyOfCapacity());
        assertThrows(
                IllegalArgumentException.class,
                () -> stupidAssistant.saveBag(new Bag(BagSize.HUGE)));
    }

    @Test
    void should_throw_error_when_cabinet_is_full() {
        StupidAssistant stupidAssistant = new StupidAssistant(createCabinetWithFullLockers(new LockerSize[]{LockerSize.BIG}, 1));
        assertThrows(InsufficientLockersException.class,
                () -> stupidAssistant.saveBag(new Bag(BagSize.BIG)));
    }

    @Test
    void should_save_bag_in_first_cabinet() {
        ArrayList<Cabinet> cabinets = new ArrayList<>();
        Cabinet cabinet1 = createCabinetWithPlentyOfCapacity();
        Cabinet cabinet2 = createCabinetWithPlentyOfCapacity();
        cabinets.add(cabinet1);
        cabinets.add(cabinet2);
        StupidAssistant stupidAssistant = new StupidAssistant(cabinets);
        Ticket ticket = stupidAssistant.saveBag(new Bag(BagSize.MEDIUM));
        assertNotNull(ticket);
    }

    @Test
    void should_save_bag_in_next_cabinet_when_first_cabinet_is_full() {
        ArrayList<Cabinet> cabinets = new ArrayList<>();
        Cabinet cabinet1 = createCabinetWithFullLockers(new LockerSize[]{LockerSize.BIG}, 1);
        Cabinet cabinet2 = createCabinetWithPlentyOfCapacity();
        cabinets.add(cabinet1);
        cabinets.add(cabinet2);
        StupidAssistant stupidAssistant = new StupidAssistant(cabinets);
        Ticket ticket = stupidAssistant.saveBag(new Bag(BagSize.BIG));
        assertNotNull(ticket);
    }

    @Test
    void should_use_lazy_assistant_to_save_bag() {
        ArrayList<Cabinet> cabinets = new ArrayList<>();
        cabinets.add(createCabinetWithPlentyOfCapacity());
        LazyAssistant lazyAssistant = new LazyAssistant(cabinets);
        Ticket ticket = lazyAssistant.saveBag(new Bag(BagSize.MEDIUM));
        assertNotNull(ticket);
    }

    @Test
    void should_save_medium_bag_in_big_locker_when_medium_locker_is_full() {
        ArrayList<Cabinet> cabinets = new ArrayList<>();
        cabinets.add(createCabinetWithFullLockers(new LockerSize[]{LockerSize.MEDIUM},5));
        LazyAssistant lazyAssistant = new LazyAssistant(cabinets);
        Ticket ticket = lazyAssistant.saveBag(new Bag(BagSize.MEDIUM));
        assertNotNull(ticket);
    }
}
