package com.twuc.bagSaving;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class LazyAssistant {
    private final ArrayList<Cabinet> cabinets;
    private final Map<BagSize, LockerSize> sizeMap = new HashMap<>();

    public LazyAssistant(ArrayList<Cabinet> cabinets) {
        this.cabinets = cabinets;
        setSizeMap();
    }

    private void setSizeMap() {
        sizeMap.put(BagSize.BIG, LockerSize.BIG);
        sizeMap.put(BagSize.MEDIUM, LockerSize.MEDIUM);
        sizeMap.put(BagSize.SMALL, LockerSize.SMALL);
    }

    public Ticket saveBag(Bag bag) {
        for (int i = 0; i < cabinets.size(); i++) {
            boolean flag = true;
            Ticket ticket = null;

            try {
                ticket = cabinets.get(i).save(bag, sizeMap.get(bag.getBagSize()));
                if (i == cabinets.size() - 1) {
                    return cabinets.get(i).save(bag, sizeMap.get(bag.getBagSize()));
                }
            } catch (InsufficientLockersException e) {
                if (bag.getBagSize() == BagSize.SMALL) {
                    ticket = cabinets.get(i).save(bag, LockerSize.MEDIUM);
                } else if (bag.getBagSize() == BagSize.MEDIUM) {
                    ticket = cabinets.get(i).save(bag, LockerSize.BIG);
                } else {
                    flag = false;
                }
            } finally {
                if (flag) {
                    return ticket;
                }
            }
        }
        return null;
    }
}
