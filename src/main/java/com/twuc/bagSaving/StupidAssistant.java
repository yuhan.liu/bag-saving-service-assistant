package com.twuc.bagSaving;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class StupidAssistant {

    private List<Cabinet> cabinets = new ArrayList<>();
    private final Map<BagSize, LockerSize> sizeMap = new HashMap<>();

    private void setSizeMap() {
        sizeMap.put(BagSize.BIG, LockerSize.BIG);
        sizeMap.put(BagSize.MEDIUM, LockerSize.MEDIUM);
        sizeMap.put(BagSize.SMALL, LockerSize.SMALL);
    }

    public StupidAssistant(Cabinet cabinet) {
        this.cabinets.add(cabinet);
        setSizeMap();
    }

    public StupidAssistant(ArrayList<Cabinet> cabinets) {
        this.cabinets = cabinets;
        setSizeMap();
    }

    public Ticket saveBag(Bag savedBag) {
        for (int i = 0; i < cabinets.size(); i++) {
            boolean flag = true;
            Ticket ticket = null;
            if (i == cabinets.size() - 1) {
                return cabinets.get(i).save(savedBag, sizeMap.get(savedBag.getBagSize()));
            }
            try {
                ticket = cabinets.get(i).save(savedBag, sizeMap.get(savedBag.getBagSize()));
            } catch (InsufficientLockersException e) {
                flag = false;
            } finally {
                if (flag) {
                    return ticket;
                }
            }
        }
        return null;
    }

    public Bag getBag(Ticket ticket) {
        return cabinets.get(0).getBag(ticket);
    }
}
